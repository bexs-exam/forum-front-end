# Aplicação Forum Front-end

Front-end que consome a api do Forum

# Observações

 - Esta aplicação foi construida em ReactJS/Material UI atualmente ela se encontra hospeda no heroku podendo ser acessada por este endereço: https://front-end-test-bexs.herokuapp.com/
 - Este projeto possuí uma variável de ambiente (**REACT_APP_URL_QUESTIONS_SERVICES**), então se faz necessário executar primeiro o projeto da api: https://gitlab.com/bexs-exam/bexs-forum-backend.git
 - Feito isso, siga os passos a baixo

# Como executar a aplicação localmente
Siga os passos a baixo para executar a aplicação localmente:

## 1. Faça o clone deste repositório em seu ambiente de desenvolvimento

    $ git clone https://gitlab.com/bexs-exam/forum-front-end.git

## 2. Instale as dependências do projeto

    $ npm install


## 3. Execute o projeto localmente

    $ npm run start-dev

