import { combineReducers } from 'redux';
import question from './Reducers/question'
import detail from './Reducers/detail'
import answers from './Reducers/answers'

export default combineReducers({
    question,
    detail,
    answers
})

