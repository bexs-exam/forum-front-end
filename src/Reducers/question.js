import { ADD_NEW_QUESTION, LIST_QUESTIONS } from '../Actions/types';


export default (state = [], action = {}) => {
    switch (action.type) {
        case ADD_NEW_QUESTION:
            return [
                ...state, 
                action.question
            ]
            
        case LIST_QUESTIONS:
            return action.questions
        

        default: return state;
    }
}