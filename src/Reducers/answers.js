import { ANSWERS_QUESTIONS } from '../Actions/types';

export default (state = [], action = {}) => {
    switch (action.type) {
        case ANSWERS_QUESTIONS:
            if (Array.isArray(action.answer))
                return action.answer
            else 
                return [...state, action.answer]
        default:
            return state;
    }
}