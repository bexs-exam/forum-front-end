import { DETAIL_QUESTION } from '../Actions/types';

export default (state = {}, action = {}) => {
    switch (action.type) {
        case DETAIL_QUESTION:
            return action.detail;
        default:
            return state;
    }
}