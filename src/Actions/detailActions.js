import { DETAIL_QUESTION , ANSWERS_QUESTIONS } from './types'


export function getDetailQuestion(detail) {
    return {
        type: DETAIL_QUESTION,
        detail
    }
}

export function getAnswersQuestion(answer) {
    return {
        type: ANSWERS_QUESTIONS,
        answer
    }
}


export function detailQuestion(detail) {
    return dispatch => {
        dispatch(getDetailQuestion(detail))
    }
}

export function detailAnswers(answer) {
    return dispatch => {
        dispatch(getAnswersQuestion(answer))
    }
}