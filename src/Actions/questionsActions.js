import axios from 'axios';
import { ADD_NEW_QUESTION, LIST_QUESTIONS } from '../Actions/types'


export function addQuestion(question) {
    return {
        type: ADD_NEW_QUESTION,
        question
    }
}

export function listQuestions(questions) {
    return {
        type: LIST_QUESTIONS,
        questions
    }
}

export function createNewQuestion(question) {
    return dispatch => {
        return axios.post(process.env.REACT_APP_URL_QUESTIONS_SERVICES + '/questions', question).then(res => {
            dispatch(addQuestion(res.data.response))
        })
    }
}

export function getQuestions() {
    return dispatch => {
        return axios.get(process.env.REACT_APP_URL_QUESTIONS_SERVICES + '/questions').then(res => {
            dispatch(listQuestions(res.data.response))           
        }).catch(err => {
        })
    }
}