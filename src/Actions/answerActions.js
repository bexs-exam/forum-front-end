import axios from 'axios';
import { getAnswersQuestion } from './detailActions';

export function newAnswer(answer) {
    return dispatch => {
        return axios.post(process.env.REACT_APP_URL_QUESTIONS_SERVICES + '/answer', answer).then(res => {
            dispatch(getAnswersQuestion(res.data.response))
            return res
        })
    }

}