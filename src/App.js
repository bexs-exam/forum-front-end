import React from 'react';
import Questions from './Components/Pages/Questions'
import QuestionsDetail from './Components/Pages/QuestionsDetail'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { connect} from 'react-redux';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Questions} />
          <Route path="/detail/:id" exact component={QuestionsDetail} />
        </Switch>
      </Router>
    )
  }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(App);