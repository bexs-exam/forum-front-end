import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { withStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withRouter } from "react-router-dom";
import { newAnswer } from '../../Actions/answerActions';
import { detailQuestion, detailAnswers } from '../../Actions/detailActions';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';


const styles = theme => ({
    gridInPaper: {
        marginBottom: '10px'
    },
});



class AnswerForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            text: ''
        }

        this.onSubmitNewAnswer = this.onSubmitNewAnswer.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmitNewAnswer(e) {
        e.preventDefault()
        const { text, user } = this.state;
        
        if (this.handleValitation()) {

            const newAnswerObject = {
                parent_id: parseInt(this.props.match.params.id),
                user: user,
                text: text
            }

            this.props.newAnswer(newAnswerObject).then(res => {
                this.props.detail.answers.push(res.data.response)
                this.manageStorage(this.props.detail)
                this.props.detailQuestion(this.props.detail)
                Swal.fire(
                    'Legal!',
                    'Você cadastrou uma pergunta',
                    'success'
                )
            }).catch(err => {
                Swal.fire(
                    'Ops!',
                    'Ouve um problema ao cadastrar sua pergunta',
                    'error'
                )
            })
        }
        else {
            Swal.fire(
                'Ops!',
                'Por favor, preencha todos os campos',
                'error'
              )
        }
    }

    handleValitation() {
        const { text, user } = this.state;
        if (text === "" || user === "") {
            return false;
        }
        else {
            return true;
        }
        
    }

    manageStorage(detail) {
        localStorage.removeItem('detail')
        localStorage.setItem('detail', JSON.stringify(detail))
    }

    render() {
        const { classes } = this.props;
        const { user, text } = this.state;
        return (
            <form noValidate onSubmit={this.onSubmitNewAnswer}>


                <Grid container className={classes.gridInPaper}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Nome de usuário"
                        fullWidth
                        disabled={false}
                        onChange={this.onChange}
                        name="user"
                        value={user}
                        variant="outlined"
                    />
                </Grid>
                <Grid container className={classes.gridInPaper}>

                    <TextField
                        id="outlined-multiline-static"
                        label="Novo comentário"
                        multiline
                        fullWidth
                        rows={4}
                        disabled={false}
                        onChange={this.onChange}
                        name="text"
                        value={text}
                        variant="outlined"
                    />

                </Grid>

                <Grid container justify="space-between" className={classes.gridInPaper}>

                    <Grid item>

                        <Button type="submit" variant="contained" color="primary">
                            Comentar
                            </Button>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

AnswerForm.propTypes = {
    detail: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {

    };
}

export default withRouter(withStyles(styles, { withTheme: true })(connect(mapStateToProps, { newAnswer, detailQuestion, detailAnswers })(AnswerForm)));