import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from "@material-ui/core/styles"
import PropTypes from 'prop-types';
import ClipLoader from "react-spinners/ClipLoader";
import Swal from 'sweetalert2'

const styles = theme => ({
    root: {
        flexGrow: 1,
        padding: '20px'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },

    gridInPaper: {
        marginBottom: '10px'
    },

    buttonsPaper: {
        marginRight: '5px'
    }

});

class QuestionForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            text: "",
            user: "",
            errors: {},
            isLoading: false
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.cleanForm = this.cleanForm.bind(this)
    }

    cleanForm() {
        this.setState({text: "", user: ""})
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()
        this.setState({
            errors: {}, isLoading: true
        })

        if (this.handleValitation()) {
            this.props.createNewQuestion({'user': this.state.user, 'text': this.state.text, 'answers': []}).then((res) => {
                this.setState( {isLoading: false})
                Swal.fire(
                    'Legal!',
                    'Você cadastrou uma pergunta',
                    'success'
                  )
            }).catch((err) => {
                Swal.fire(
                    'Ops!',
                    'Ouve um problema ao cadastrar sua pergunta',
                    'error'
                  )
                this.setState( {isLoading: false})
            })
        }
        else {
            Swal.fire(
                'Ops!',
                'Por favor, preencha todos os campos',
                'error'
              )
            this.setState( {isLoading: false})
        }
    }

    handleValitation() {
        const { text, user } = this.state;
        if (text === "" || user === "") {
            return false;
        }
        else {
            return true;
        }
        
    }

    render() {
        const { classes } = this.props;
        const { text, user, isLoading } = this.state;
        return (
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <form noValidate onSubmit={this.onSubmit}>
                        <Grid container className={classes.gridInPaper}>
                            <TextField
                                id="outlined-multiline-static"
                                label="Nome de usuário"
                                fullWidth
                                disabled={isLoading}
                                onChange={this.onChange}
                                name="user"
                                value={user}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid container className={classes.gridInPaper}>
                            <TextField
                                id="outlined-multiline-static"
                                label="Nova pergunta"
                                multiline
                                fullWidth
                                rows={4}
                                disabled={isLoading}
                                onChange={this.onChange}
                                name="text"
                                value={text}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid container justify="flex-end">
                            <Grid item className={classes.buttonsPaper}>
                                <Button disabled={isLoading} onClick={this.cleanForm} variant="contained" color="default">
                                    Limpar
                        </Button>
                            </Grid>
                            <Grid item>
                                <Button disabled={isLoading} type="submit" variant="contained" color="primary">
                                    {
                                        isLoading ? (
                                            <ClipLoader
                                                size={25}
                                                color={"#123abc"} 
                                            />
                                        ) : 'Enviar'
                                    }
                                    
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>

            </Grid>
        );
    }
}

QuestionForm.propTypes = {
    createNewQuestion: PropTypes.func.isRequired
}

export default withStyles(styles, { withTheme: true })(QuestionForm);