import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from "@material-ui/core/styles"
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { Redirect } from 'react-router-dom';

const styles = theme => ({
    gridInPaper: {
        marginBottom: '10px'
    },
});

class AnswerPart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            back: false
        }

        this.handleBackAction = this.handleBackAction.bind(this)

    }

    handleBackAction() {
        this.setState({ back: true })
    }


    render() {
        const { classes, answer_text, answer_author } = this.props;
        const { back } = this.state;

        if (back) {
            return <Redirect to="/" />
        }
        return (
            <>
                <Grid container justify="space-between" className={classes.gridInPaper}>
                    <Grid item>
                        <Typography variant="caption">
                            {answer_author}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <IconButton onClick={() => this.handleBackAction()} color="primary" aria-label="add to shopping cart">
                            <KeyboardBackspaceIcon />
                        </IconButton>
                    </Grid>
                </Grid>
                <Grid container className={classes.gridInPaper}>

                    <Typography variant="body1">
                        {answer_text}
                    </Typography>


                </Grid>
            </>
        );
    }
}

function mapStateToProps(state) {
    return {
        answer_text: state.detail.text,
        answer_author: state.detail.user
    };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(AnswerPart));
