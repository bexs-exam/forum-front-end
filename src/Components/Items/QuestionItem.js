import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CommentIcon from '@material-ui/icons/Comment';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

class QuestionItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            details: false
        }

        this.handleDetail = this.handleDetail.bind(this)
    }

    handleDetail(detail) {
        localStorage.removeItem('detail')
        localStorage.setItem('detail', JSON.stringify(detail))
        this.props.detailAnswers(detail.answers)
        this.props.detailQuestion(detail)
        this.setState({ details: true })
    }

    render() {
        const { question } = this.props;
        const { details } = this.state;
        

        if (details) {
            return <Redirect to={"/detail/"+ question.id} />
        }

        return (
            <ListItem button onClick={() => this.handleDetail(question)}>
                <ListItemText primary={question.text} />
                <ListItemSecondaryAction>
                    <Grid container>
                        <Grid item>
                            ({question.answers.length})
                        </Grid>
                        <Grid item>
                            <CommentIcon />
                        </Grid>
                    </Grid>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}

QuestionItem.propTypes = {
    detailQuestion: PropTypes.func.isRequired
}

export default connect(null)(QuestionItem);