import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class AnswerItem extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }


    render() {
        const { answer } = this.props;
        return (
            <ListItem>
                <ListItemText primary={answer.text} />
            </ListItem>
        );
    }
}


export default connect(null)(AnswerItem);