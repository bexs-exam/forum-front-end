import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { withStyles } from "@material-ui/core/styles"
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AnswerList from '../Lists/AnswersList'
import { connect } from 'react-redux';
import AnswerPart from '../Parts/AnswerPart';
import AnswerForm from '../Forms/AnswerForm';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '20px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

  gridInPaper: {
    marginBottom: '10px'
  },

  buttonsPaper: {
    marginRight: '5px'
  }

});


class QuestionsDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }

  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              Forum
            </Typography>
          </Toolbar>
        </AppBar>
        <CssBaseline />
        <Container maxWidth="lg">
          <Typography component="div" >
            <div className={classes.root}>
              <Grid container spacing={3}>
                <React.Fragment>
                  <CssBaseline />
                  <Container maxWidth="lg">
                    <Typography component="div" >
                      <div className={classes.root}>
                        <Grid container spacing={3}>
                          <Grid item xs={12}>
                            <Paper className={classes.paper}>
                              <AnswerPart />
                              <AnswerForm detail={this.props.detail}/>
                            </Paper>
                          </Grid>
                          <AnswerList/> 
                        </Grid>
                      </div>
                    </Typography>
                  </Container>
                </React.Fragment>
              </Grid>
            </div>
          </Typography>
        </Container>
      </React.Fragment>
    );
  }
}


function mapStateToProps(state) {
  return {
      detail: state.detail
  };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps,)(QuestionsDetail));

