import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { withStyles } from "@material-ui/core/styles"
import Grid from '@material-ui/core/Grid';
import QuestionForm from '../Forms/QuestionForm';
import QuestionList from '../Lists/QuestionsList'
import { createNewQuestion } from '../../Actions/questionsActions'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '20px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

  gridInPaper: {
    marginBottom: '10px'
  },

  buttonsPaper: {
    marginRight: '5px'
  }

});


class Questions extends React.Component {
  render() {
    const { classes, createNewQuestion } = this.props;
    return (
      <React.Fragment>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              Forum
            </Typography>
          </Toolbar>
        </AppBar>
        <CssBaseline />
        <Container maxWidth="lg">
          <Typography component="div"  >
            <div className={classes.root}>
              <Grid container spacing={3}>
                <QuestionForm createNewQuestion={createNewQuestion} />
                <QuestionList />
              </Grid>
            </div>
          </Typography>
        </Container>
      </React.Fragment>
    );
  }
}

Questions.propTypes = {
  createNewQuestion: PropTypes.func.isRequired
}

export default withStyles(styles, { withTheme: true })(connect(null, { createNewQuestion })(Questions));

