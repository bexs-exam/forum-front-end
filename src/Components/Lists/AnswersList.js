import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from "@material-ui/core/styles"
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import AnswerItem from '../Items/AnswerItem';
import PropTypes from 'prop-types';


const styles = theme => ({
    root: {
        flexGrow: 1,
        padding: '20px'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },

    gridInPaper: {
        marginBottom: '10px'
    },

    buttonsPaper: {
        marginRight: '5px'
    }

});

class AnswersList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true
        }
    }


    render() {
        const { classes, answers } = this.props;
        


        const anwsers_itens = answers.map((answer, index) => {
            return (<AnswerItem key={answer.id} answer={answer} />)
        })
        return (
            <Grid item xs={12}>
                <Paper >
                    <div className={classes.root}>
                        <List component="nav" aria-label="main mailbox folders">
                            {answers.length ? anwsers_itens : 'Esta pergunta ainda não tem respostas'}
                        </List>
                    </div>
                </Paper>
            </Grid>
        );
    }
}

AnswersList.propTypes = {
    answers: PropTypes.array.isRequired
}


function mapStateToProps(state) {
    
    return {
        answers: state.answers
    };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps,)(AnswersList));