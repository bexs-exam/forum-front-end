import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from "@material-ui/core/styles"
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import QuestionItem from '../Items/QuestionItem';
import ClipLoader from "react-spinners/ClipLoader";
import { detailQuestion, detailAnswers } from '../../Actions/detailActions';
import PropTypes from 'prop-types';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '20px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

  gridInPaper: {
    marginBottom: '10px'
  },

  buttonsPaper: {
    marginRight: '5px'
  }

});

class QuestionsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }


  render() {
    const { classes, questions, detailQuestion, detailAnswers } = this.props;
    const questions_itens = questions.map((question, index) => {
      return (<QuestionItem detailQuestion={detailQuestion} detailAnswers={detailAnswers} key={question.id} question={question} />)
    })

    return (
      <Grid item xs={12}>
        <Paper >
          <div className={classes.root}>
            <List component="nav" aria-label="main mailbox folders">
              {questions.length ? questions_itens : (
                <ClipLoader
                  size={25}
                  color={"#123abc"}
                />
              )}
            </List>
          </div>
        </Paper>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    questions: state.question
  };
}

QuestionItem.propTypes = {
  detailQuestion: PropTypes.func.isRequired,
  detailAnswers: PropTypes.func.isRequired
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, { detailQuestion, detailAnswers })(QuestionsList));